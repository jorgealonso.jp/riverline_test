<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="row ml-0 mr-0">
            <div class="position-fixed bg-dark col-3" style="height: 100vh;">
                <div class="bg-light text-center mb-3" style="height: 35%; vertical-align: center;">
                    <h3 class="align-center">{{ Auth::user()->name }}</h3>
                </div>
                <div>
                    <a class="btn btn-primary btn-lg btn-block" href="{{ route('request_vacation') }}">Solicitar Vacaciones</a>
                    @if(auth::user()->id == 1)
                    <a class="btn btn-primary btn-lg btn-block" href="{{ route('gestione_vacation') }}">Gestionar solicitudes</a>
                    @endif
                    <a class="btn btn-primary btn-lg btn-block" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
            </div>
            <div class="col-2">&nbsp;</div>
            <main class="col-7 container">
                @yield('content')
            </main>    
        </div>
        
    </div>
</body>
</html>
