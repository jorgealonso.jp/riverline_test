@extends('layouts.dashboard')

@section('content')
	<h1>Solicitudes</h1>
	<table class="table">
		<tr>
			<th>Inicio</th>
			<th>Fin</th>
		</tr>
		@foreach($requests as $vacation)
		<tr>
			<td>{{ $vacation->begin }}</td>
			<td>{{ $vacation->end }}</td>
			<td><a href="{{ route('show_vacation_request', ['id' => $vacation->id]) }}">Ver solicitud</a></td>
		</tr>
		@endforeach
	</table>
@endsection