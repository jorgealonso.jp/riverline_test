@extends('layouts.dashboard')
@php
	// set_locate('es');
@endphp
@section('content')
	<h1 class="text-center">Solicitud de vacaciones</h1>
	<div class="container">
		<h3 class="text-center text-muted">Indica el inicio y el fin de tus vacaciones</h3>
		<form action="{{ route('request_vacation_store') }}" method="POST">
			@csrf
			<div class="border radious row justify-content-center">
				<span class="text-center col-12">Indica la fecha de inicio inicio</span>
				<div class="form-group col-3">
					<label for="begin_day" class="form-label">Dia</label>
					<select class="form-control" name="begin_day">
						@for($i=1; $i <= 31; $i++)
							<option>{{ $i }}</option>
						@endfor
					</select>
				</div>
				<div class="form-group col-3">
					<label for="begin_month" class="form-label">Mes</label>
					<select class="form-control" name="begin_month">
						<option value="1">Enero</option>
						<option value="2">Febrero</option>
						<option value="3">Marzo</option>
						<option value="4">Abril</option>
						<option value="5">Mayo</option>
						<option value="6">Junio</option>
						<option value="7">Julio</option>
						<option value="8">Agosto</option>
						<option value="9">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					</select>
				</div>
				<div class="form-group col-3">
					<label for="begin_year" class="form-label">Año</label>
					<select class="form-control" name="begin_year">
						@for($i=0; $i< 10; $i++)
						<option>{{ (integer)date('Y') + $i }}</option>
						@endfor
					</select>
				</div>
			</div>
			<div class="border radious row justify-content-center mt-12">
				<span class="text-center col-12">Indica la fecha de termino</span>
				<div class="form-group col-3">
					<label for="end_day" class="form-label">Dia</label>
					<select class="form-control" name="end_day">
						@for($i=1; $i <= 31; $i++)
							<option>{{ $i }}</option>
						@endfor
					</select>
				</div>
				<div class="form-group col-3">
					<label for="end_month" class="form-label">Mes</label>
					<select class="form-control" name="end_month">
						<option value="1">Enero</option>
						<option value="2">Febrero</option>
						<option value="3">Marzo</option>
						<option value="4">Abril</option>
						<option value="5">Mayo</option>
						<option value="6">Junio</option>
						<option value="7">Julio</option>
						<option value="8">Agosto</option>
						<option value="9">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					</select>
				</div>
				<div class="form-group col-3">
					<label for="end_year" class="form-label">Año</label>
					<select class="form-control" name="end_year">
						@for($i=0; $i< 10; $i++)
						<option>{{ (integer)date('Y') + $i }}</option>
						@endfor
					</select>
				</div>
			</div>
			<button class="btn btn-success btn-lg btn-block" type="submit">Enviar solicitud</button>
		</form>
	</div>
@endsection