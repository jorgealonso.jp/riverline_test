@extends('layouts.dashboard')

@section('content')
	<h1>Solicitudes</h1>
	<table class="table">
		<tr>
			<th>Inicio</th>
			<th>Fin</th>
			<th>Resolución</th>
		</tr>

		<tr>
			<td>{{ $vacation->begin }}</td>
			<td>{{ $vacation->end }}</td>
			<td>{{ $vacation->resolution }}</td>
			
		</tr>
	</table>
@endsection