<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\User;

class VacationRequest extends Model
{
    protected $fillable = [ 'id', 'begin', 'end', 'user_id', 'gestioned', 'resolution'];

    public function findUser(){
    	$user = User::find($this->user_id);
    	$this->name = $user->name;
    	return $this;
    }
}
