<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\VacationRequest;
use Illuminate\Support\Facades\Auth;

class VacationRequestController extends Controller
{
    public function index(){
        return view('vacation_request.index')->with('requests', VacationRequest::where('user_id', Auth::user()->id)->get());
    }

    public function create(){
        return view('vacation_request.create');
    }

    public function store(Request $request){
        VacationRequest::create([
            'begin' => $request->begin_year.'-'.$request->begin_month.'-'.$request->begin_day.' 00:00:00',
            'end' => $request->end_year.'-'.$request->end_month.'-'.$request->end_day.' 00:00:00',
            'user_id' => Auth::user()->id,
            'gestioned' => 0,
            'resolution' => 'No ha sido revisada'
        ]);
        return redirect()->route('home');
    }

    public function show($id){
        return view('vacation_request.show')->with('vacation', VacationRequest::find($id));
    }

    public function edit($id){

    }

    public function update($id, Request $request){

    }

    public function destroy($id){

    }

    public function gestion(){
        return view('vacation_request.gestion')->with('requests', VacationRequest::where('gestioned', 0)->get());
    }
}
