<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'VacationRequestController@index')->name('home');
Route::get('/make_request/create', 'VacationRequestController@create')->middleware(['auth'])->name('request_vacation');
Route::post('/make_request', 'VacationRequestController@store')->middleware(['auth'])->name('request_vacation_store');
Route::get('/make_request/{id}', 'VacationRequestController@show')->middleware(['auth'])->name('show_vacation_request');
Route::get('gestion', 'VacationRequestController@gestion')->middleware(['auth', 'isauth'])->name('gestione_vacation');
