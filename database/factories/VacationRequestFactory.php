<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\VacationRequest;
use Faker\Generator as Faker;

$factory->define(VacationRequest::class, function (Faker $faker) {
    return [
        'begin' => $faker->dateTimeBetween('now', '+2 months'),
        'end' => $faker->dateTimeBetween('now', '+2 months'),
        'user_id' => $faker->numberBetween(1,52),
        'gestioned' => $faker->randomDigit % 2 ? 1 : 0,
        'resolution' => $faker->randomDigit % 2 ? 'No se ha gestionado' : 'Vacaciones Gestionadas',
    ];
});
