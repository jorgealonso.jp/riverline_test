<?php

use Illuminate\Database\Seeder;

class VacationRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\VacationRequest::class, 250)->create();
    }
}
