<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	App\User::create(['name' => 'Administrator', 'email' => 'admin@admin.com', 'password' => bcrypt('secret'), 'type_user' => 1]);
        App\User::create(['name' => 'User', 'email' => 'user@user.com', 'password' => bcrypt('secret')]);
        factory(App\User::class, 50)->create();
    }
}
